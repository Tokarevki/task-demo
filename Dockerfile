FROM openjdk:11-jre-slim
ARG JAR_FILE
ARG FILE_COPY_PATH
ARG OUTPUT_DIRECTORY
ADD target/${JAR_FILE} app.jar
ADD docker/input/task2_input ${FILE_COPY_PATH}
ENTRYPOINT ["java","-jar","/app.jar", "-Dtask2.outputDirectory=${OUTPUT_DIRECTORY}"]

package com.example.task2.controller.model;

import java.nio.file.Path;
import lombok.Data;
import lombok.Value;

@Value
public class Response {

  Path resultFile;
}

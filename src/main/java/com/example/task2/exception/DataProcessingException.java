package com.example.task2.exception;

public class DataProcessingException extends RuntimeException {

  public DataProcessingException(String message) {
    super(message);
  }

}

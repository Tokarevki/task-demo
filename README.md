# How to use:

Set correct absolute path to input file (inputFilePath) in src/main/resources/application.yml 
or set FILE_PATH environment variable.

Run this command to build docker
```
mvn clean package docker:build
```
Run this command to start it afterwards (or run container with docker)
```
mvn docker:start
```
This will run docker container on port 8080

## Details:
Target file is on src/main/resources/task2_input, it will be bundled with a jar.

## Invocation:
```
POST http://localhost:8080/processor/task
Content-Type: application/json

{
    "fileName": "result.txt"
}
```

Returns result in json with file on docker:

```
{
  "resultFile": "file:///root/task2/result.txt"
}
```

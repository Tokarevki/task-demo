package com.example.task2.controller;

import com.example.task2.controller.model.Request;
import com.example.task2.controller.model.Response;
import com.example.task2.service.TaskProcessingService;
import java.nio.file.Path;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("processor")
public class ProcessingController {

  private final TaskProcessingService taskProcessingService;

  @PostMapping("task")
  public Response process(@RequestBody Request request) {
    return new Response(taskProcessingService.process(request.getFileName()));
  }

}

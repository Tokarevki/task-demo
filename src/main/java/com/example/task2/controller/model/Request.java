package com.example.task2.controller.model;

import lombok.Data;

@Data
public class Request {

  private String fileName;
}

package com.example.task2.service;

import java.nio.file.Path;

public interface TaskProcessingService {

  /**
   * Processes file and creates a new result file with provided name.
   * @param targetFileName - name of result file
   * @return path to result file.
   */
  Path process(String targetFileName);
}

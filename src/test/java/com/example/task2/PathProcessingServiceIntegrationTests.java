package com.example.task2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.task2.exception.DataProcessingException;
import com.example.task2.service.PathTaskProcessingService;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PathProcessingServiceIntegrationTests {

  @Autowired
  private PathTaskProcessingService processingService;

  @Test
  void should_process_file() {
    final String expectedFileName = "test";
    final Path actual = processingService.process(expectedFileName);

    assertEquals(expectedFileName, actual.getFileName().toString());
  }

  @Test
  void should_throw_exception_on_missing_file_name() {
    assertThrows(DataProcessingException.class,
        () -> processingService.process(null));
  }

  @Test
  void should_throw_exception_on_blank_file_name() {
    assertThrows(DataProcessingException.class,
        () -> processingService.process(""));
  }

}

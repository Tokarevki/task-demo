package com.example.task2.controller.config;

import com.example.task2.exception.DataProcessingException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler()
  protected ResponseEntity<RestExceptionModel> handleDomainException(DataProcessingException ex) {
    return ResponseEntity.badRequest().body(new RestExceptionModel(ex.getMessage()));
  }
}

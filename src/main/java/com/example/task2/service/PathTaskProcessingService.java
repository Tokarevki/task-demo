package com.example.task2.service;

import com.example.task2.exception.DataProcessingException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PathTaskProcessingService implements TaskProcessingService {

  private final Pattern pattern;
  private final Path resourceFile;
  private final Path outputDirectory;

  public PathTaskProcessingService(@Value("${task2.inputFilePath}") String inputFilePath,
      @Value("${task2.pattern}") String fileLinePattern,
      @Value("${task2.outputDirectory}") String outputDirectory) {
    this.resourceFile = Path.of(inputFilePath);
    this.outputDirectory = Path.of(outputDirectory);
    this.pattern = Pattern.compile(fileLinePattern);
  }

  @Override
  public Path process(String fileName) {
    final Path targetPath = this.constructTargetPath(fileName);
    final Stream<String> lines = this.getLinesAsStream();
    Map<String, List<DataProcessingHelper>> groupedLinesMap = this.populateDataMap(lines);

    if (groupedLinesMap.isEmpty()) {
      throw new DataProcessingException("Processing of files did not return anything");
    }

    this.writeResultToFile(targetPath, groupedLinesMap);

    return targetPath;
  }

  private Path constructTargetPath(String fileName) {
    if (Optional.ofNullable(fileName).filter(s -> !s.isBlank()).isEmpty()) {
      throw new DataProcessingException("File name is missing");
    }
    return Path.of(outputDirectory.toString(), "task2", fileName);
  }

  private void writeResultToFile(Path targetPath, Map<String, List<DataProcessingHelper>> map) {
    prepareDirectories(targetPath);
    map.values()
        .stream()
        .map(dataHelpers -> {
          final String changingParts = dataHelpers.stream()
              .map(DataProcessingHelper::getChangingPart)
              .distinct()
              .collect(Collectors.joining(", "));

          return dataHelpers.stream()
              .map(DataProcessingHelper::getLine)
              .distinct()
              .collect(Collectors.joining("\n", "",
                  String.format("\nThe changing word was: %s\n\n\n", changingParts)));
        })
        .forEach(writeGroup(targetPath));
  }

  private void prepareDirectories(Path targetPath) {
    try {
      Files.createDirectories(targetPath.getParent());
      Files.deleteIfExists(targetPath);
      Files.createFile(targetPath);
    } catch (IOException e) {
      log.error("Error during creation of result file", e);
      throw new DataProcessingException("Unable to create result file");
    }
  }

  private Consumer<String> writeGroup(Path targetPath) {
    return group -> {
      try {
        Files.writeString(targetPath, group, StandardOpenOption.APPEND);
      } catch (IOException e) {
        log.error("Error during parsing of file", e);
        throw new DataProcessingException("Encountered error during file saving");
      }
    };
  }

  private Map<String, List<DataProcessingHelper>> populateDataMap(Stream<String> lines) {
    Map<String, List<DataProcessingHelper>> result = new TreeMap<>();

    lines.forEach(line -> {
      final Matcher matcher = pattern.matcher(line);
      if (matcher.matches()) {
        final String name = matcher.group("name");
        final String group = matcher.group("group");
        List<DataProcessingHelper> items = result.computeIfAbsent(group, k -> new LinkedList<>());
        items.add(new DataProcessingHelper(name, line));
      }
    });

    return result;
  }

  private Stream<String> getLinesAsStream() {
    try {
      return Files.lines(resourceFile);
    } catch (IOException e) {
      log.error("Exception during file read", e);
      throw new DataProcessingException("Unable to read input file from classpath");
    }
  }

  @lombok.Value
  private static class DataProcessingHelper {

    String changingPart;
    String line;
  }
}

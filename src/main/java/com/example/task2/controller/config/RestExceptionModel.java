package com.example.task2.controller.config;

import lombok.Data;
import lombok.Value;

@Value
public class RestExceptionModel {

  String message;
}
